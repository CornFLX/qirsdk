#ifndef LIVEIRSDK_H
#define LIVEIRSDK_H

#include "abstractirsdk.h"

#include <tchar.h>
#include <time.h>

#include <QHash>

class LiveIrSdk : public AbstractIrSdk
{
    Q_OBJECT

    typedef void* Handle;

public:
    LiveIrSdk(QObject* parent = 0);
    ~LiveIrSdk();

    bool isConnected() const;
    bool waitForData(int timeout = WAIT_DATA_TIMEOUT);

    int getVarIdx(const char* name);
    VarType getVarType(int idx);

    int getVarCount(int idx);
    using AbstractIrSdk::getVarCount;

    bool getVarBool(int idx, int entry = 0);
    using AbstractIrSdk::getVarBool;

    int getVarInt(int idx, int entry = 0);
    using AbstractIrSdk::getVarInt;

    float getVarFloat(int idx, int entry = 0);
    using AbstractIrSdk::getVarFloat;

    double getVarDouble(int idx, int entry = 0);
    using AbstractIrSdk::getVarDouble;

    const char* getSessionInfo() const;

protected:
    bool initialize();
    bool waitForDataReady(int timeout);
    bool getNewData();

    void shutdown();

    const VarHeader* getVarHeaderEntry(int index) const;

    Handle m_data_valid_event;
    Handle m_memmap;

    const char* m_shared_mem;

    int m_last_tick_count;
    bool m_initialized;

    time_t m_last_valid_time;

    char* m_data;
    int m_data_len;
    int m_status_id;

    static const int SHARED_MEM_SIZE = sizeof(Header) + 0x20000 + (4096 * sizeof(VarHeader)) + (MAX_BUFS * (4096 * 6));

    QHash<const char*, int> m_varname_idx;

    bool isValidIdx(int idx) const;
    bool isValidOffset(const VarHeader* vh, int entry) const;
};

#endif // LIVEIRSDK_H
