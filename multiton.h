#ifndef MULTITON_H
#define MULTITON_H

#include <QHash>

template <class Key, class T>
class Multiton
{
public:
    template <class... Targs>
    static T* ptr(const Key& key, Targs... args)
    {
        if (m_instances[key] == nullptr) {
            m_instances[key] = new T(key, args...);
        }

        return m_instances[key];
    }

    template <class... Targs>
    static T& ref(const Key& arg, Targs... args)
    {
        return *(ptr(arg, args...));
    }

    static void destroy()
    {
        for (T* instance : m_instances) {
            delete instance;
        }
        m_instances.clear();
    }

protected:
    explicit Multiton(const Key& key) : m_key(key)
    {
        Q_ASSERT(m_instances[key] == nullptr);

        m_instances[key] = static_cast<T*>(this);
    }

    virtual ~Multiton()
    {
        m_instances.remove(m_key);
    }

private:
    static QHash<Key, T*> m_instances;
    Key m_key;
};

template <class Key, class T>
QHash<Key, T*> Multiton<Key, T>::m_instances;

#endif // MULTITON_H
