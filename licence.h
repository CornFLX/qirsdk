#ifndef LICENCE_H
#define LICENCE_H

#include <QString>

class Licence
{
public:
    Licence();

    const QString& type() const;
    double safetyRating() const;

    operator QString() const;

private:
    QString m_type;
    double m_safety_rating;
};

#endif // LICENCE_H
