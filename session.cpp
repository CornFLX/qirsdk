#include "session.h"

Session::Session(const QString &type, int time_elapsed, int laps_elapsed, int time_remain, int laps_remain)
    : m_type(type), m_time_remain(time_remain), m_time_elapsed(time_elapsed), m_laps_remain(laps_remain), m_laps_elapsed(laps_elapsed)
{
}

const QString &Session::type() const
{
    return m_type;
}

int Session::timeRemain() const
{
    return m_time_remain;
}

int Session::lapsRemain() const
{
    return m_laps_remain;
}

int Session::timeElapsed() const
{
    return m_time_elapsed;
}

int Session::lapsElapsed() const
{
    return m_laps_elapsed;
}
