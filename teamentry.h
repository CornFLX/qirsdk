#ifndef TEAMENTRY_H
#define TEAMENTRY_H

#include "entry.h"

class TeamEntry : public Entry
{
public:
    TeamEntry(DriverList drivers, QString name, Car* car);

    TeamEntry(const TeamEntry&) = delete;
    TeamEntry operator=(const TeamEntry&) = delete;

    Driver* currentDriver() const;
    DriverList drivers() const;
    int irating() const;
    const QString& name() const;

    void addDriver(Driver* driver);

private:
    DriverList m_drivers;
    QString m_name;
};

#endif // TEAMENTRY_H
