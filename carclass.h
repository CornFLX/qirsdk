#ifndef CARCLASS_H
#define CARCLASS_H

#include "multiton.h"

#include <QColor>
#include <QString>

class CarClass : public Multiton<int, CarClass>
{
public:
    int id() const;
    const QString& name() const;
    const QColor& color() const;

    friend class Multiton<int, CarClass>;

protected:
    CarClass(int id, QString name, QColor color);
    CarClass(int id, QString name);

private:
    int m_id;
    QString m_name;
    QColor m_color;
};

#endif // CARCLASS_H
