#ifndef SINGLEENTRY_H
#define SINGLEENTRY_H

#include "entry.h"

class SingleEntry : public Entry
{
public:
    SingleEntry(Driver* drv, Car* car);

    SingleEntry(const SingleEntry&) = delete;
    SingleEntry operator=(const SingleEntry&) = delete;

    Driver* currentDriver() const;
    DriverList drivers() const;
    int irating() const;
    const QString& name() const;

private:
    Driver* m_driver;
};

#endif // SINGLEENTRY_H
