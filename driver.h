#ifndef DRIVER_H
#define DRIVER_H

#include <QString>
#include "licence.h"

class Driver
{
public:
    Driver(QString name="Unknown driver", int irating=0);

    Driver(const Driver&) = delete;
    Driver operator=(const Driver&) = delete;

    const QString& name() const;
    int irating() const;

    QString licence() const;
    double safetyRating() const;
    QString licenceType() const;

private:
    QString m_name;
    int m_irating;
    Licence m_licence;
};

#endif // DRIVER_H
