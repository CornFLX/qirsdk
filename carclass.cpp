#include "carclass.h"


CarClass::CarClass(int id, QString name, QColor color)
    : Multiton(id), m_id(id), m_name(name), m_color(color)
{
}

CarClass::CarClass(int id, QString name)
    : CarClass(id, name, QColor())
{
}

int CarClass::id() const
{
    return m_id;
}

const QString &CarClass::name() const
{
    return m_name;
}

const QColor &CarClass::color() const
{
    return m_color;
}
