#include "teamentry.h"

TeamEntry::TeamEntry(Entry::DriverList drivers, QString name, Car *car)
    : Entry(car), m_drivers(drivers), m_name(name)
{
}

Entry::DriverList TeamEntry::drivers() const
{
    return DriverList(m_drivers);
}

int TeamEntry::irating() const
{
    int sum = 0;
    for (Driver* driver : m_drivers) {
        sum += driver->irating();
    }

    return sum/m_drivers.size();
}

const QString &TeamEntry::name() const
{
    return m_name;
}

void TeamEntry::addDriver(Driver *driver)
{
    m_drivers.append(driver);
}
