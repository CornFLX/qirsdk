#include "diskirsdk.h"

#include <QDebug>
#include <QThread>

DiskIrSdk::DiskIrSdk(QObject *parent) :
    AbstractIrSdk(parent), m_ibt_file(0), m_subheader(0), m_var_headers(0), m_var_buf(0), m_session_info(0)
{
    m_header = new AbstractIrSdk::Header;
    m_subheader = new SubHeader;

    memset(m_header, 0, sizeof(*m_header));
    memset(m_subheader, 0, sizeof(*m_subheader));
}

DiskIrSdk::DiskIrSdk(const char *ibt_path, QObject *parent) : DiskIrSdk(parent)
{
    if (!openFile(ibt_path)) qCritical() << "Unable to open " << ibt_path;
}

DiskIrSdk::~DiskIrSdk()
{
    closeFile();
}

bool DiskIrSdk::isFileOpen() const
{
    return m_ibt_file != 0;
}

bool DiskIrSdk::openFile(const char *path)
{
    closeFile();

    m_ibt_file = fopen(path, "rb");
    if (!m_ibt_file) return false;

    if (fread(m_header, 1, sizeof(*m_header), m_ibt_file) != sizeof(*m_header)) return false;
    if (fread(m_subheader, 1, sizeof(*m_subheader), m_ibt_file) != sizeof(*m_subheader)) return false;

    m_session_info = new char[m_header->session_info_len];
    fseek(m_ibt_file, m_header->session_info_offset, SEEK_SET);
    if (fread(m_session_info, 1, m_header->session_info_len, m_ibt_file) != (size_t)m_header->session_info_len) return false;
    m_session_info[m_header->session_info_len-1] = '\0';

    m_var_headers = new VarHeader[m_header->num_vars];
    fseek(m_ibt_file, m_header->var_header_offset, SEEK_SET);
    size_t len = m_header->num_vars * sizeof(VarHeader);
    if (fread(m_var_headers, 1, len, m_ibt_file) != len) return false;

    m_var_buf = new char[m_header->buf_len];
    fseek(m_ibt_file, m_header->var_buf[0].buf_offset, SEEK_SET);

    return true;
}

void DiskIrSdk::closeFile()
{
    if (m_subheader) delete m_subheader;
    m_subheader = new SubHeader;

    if (m_var_buf) delete []m_var_buf;
    m_var_buf = 0;

    if (m_var_headers) delete []m_var_headers;
    m_var_headers = 0;

    if (m_session_info) delete []m_session_info;
    m_session_info = 0;

    if (m_ibt_file) fclose(m_ibt_file);
    m_ibt_file = 0;
}

bool DiskIrSdk::isConnected() const
{
    return isFileOpen();
}

bool DiskIrSdk::waitForData(int timeout)
{
    if (m_ibt_file) {
        QThread::msleep(timeout);
        return fread(m_var_buf, 1, m_header->buf_len, m_ibt_file) == (size_t)m_header->buf_len;
    }

    return false;
}

int DiskIrSdk::getVarIdx(const char *namen)
{
    if (m_ibt_file && namen) {
        for (int idx = 0; idx < m_header->num_vars; ++idx) {
            if (0 == strncmp(namen, m_var_headers[idx].name, MAX_STRING)) {
                return idx;
            }
        }
    }

    return -1;
}

AbstractIrSdk::VarType DiskIrSdk::getVarType(int idx)
{
    if (m_ibt_file) {
        Q_ASSERT(isValidIdx(idx));

        return (VarType)m_var_headers[idx].type;
    }

    return CHAR;
}

int DiskIrSdk::getVarCount(int idx)
{
    if (m_ibt_file) {
        Q_ASSERT(isValidIdx(idx));

        return m_var_headers[idx].count;
    }

    return 0;
}

bool DiskIrSdk::getVarBool(int idx, int entry)
{
    if (m_ibt_file) {
        Q_ASSERT(isValidEntry(idx, entry));

        const char* data = m_var_buf + m_var_headers[idx].offset;
        switch (m_var_headers[idx].type) {
        // 1 byte
        case CHAR:
        case BOOL:
            return (((const char*)data)[entry]) != 0;

        // 4 bytes
        case INT:
        case BITFIELD:
            return (((const int*)data)[entry]) != 0;

        // test float/double for greater than 1.0 so that
        // we have a chance of the being usefull
        // technically there is no right conversion...
        case FLOAT:
            return (((const float*)data)[entry]) >= 1.0;

        // 8 bytes
        case DOUBLE:
            return (((const double*)data)[entry]) >= 1.0;
        }
    }

    return false;
}

int DiskIrSdk::getVarInt(int idx, int entry)
{
    if (m_ibt_file) {
        Q_ASSERT(isValidEntry(idx, entry));

        const char* data = m_var_buf + m_var_headers[idx].offset;
        switch (m_var_headers[idx].type) {
        case CHAR:
        case BOOL:
            return (int)(((const char*)data)[entry]);

        case INT:
        case BITFIELD:
            return (int)(((const int*)data)[entry]);

        case FLOAT:
            return (int)(((const float*)data)[entry]);

        case DOUBLE:
            return (int)(((const double*)data)[entry]);
        }
    }

    return 0;
}

float DiskIrSdk::getVarFloat(int idx, int entry)
{
    if (m_ibt_file) {
        Q_ASSERT(isValidEntry(idx, entry));

        const char* data = m_var_buf + m_var_headers[idx].offset;
        switch (m_var_headers[idx].type) {
        case CHAR:
        case BOOL:
            return (float)(((const char*)data)[entry]);

        case INT:
        case BITFIELD:
            return (float)(((const int*)data)[entry]);

        case FLOAT:
            return (float)(((const float*)data)[entry]);

        case DOUBLE:
            return (float)(((const double*)data)[entry]);
        }
    }

    return 0.0f;
}

double DiskIrSdk::getVarDouble(int idx, int entry)
{
    if (m_ibt_file) {
        Q_ASSERT(isValidEntry(idx, entry));

        const char* data = m_var_buf + m_var_headers[idx].offset;
        switch (m_var_headers[idx].type) {
        case CHAR:
        case BOOL:
            return (double)(((const char*)data)[entry]);

        case INT:
        case BITFIELD:
            return (double)(((const int*)data)[entry]);

        case FLOAT:
            return (double)(((const float*)data)[entry]);

        case DOUBLE:
            return (double)(((const double*)data)[entry]);
        }
    }

    return 0.0;
}

const char *DiskIrSdk::getSessionInfo() const
{
    return m_session_info;
}

const AbstractIrSdk::VarHeader *DiskIrSdk::getVarHeaderEntry(int index) const
{
    if (!isValidIdx(index)) return 0;
    return &m_var_headers[index];
}

bool DiskIrSdk::isValidIdx(int idx) const
{
    return (idx >= 0 && idx < m_header->num_vars);
}

bool DiskIrSdk::isValidEntry(int idx, int entry) const
{
    return isValidIdx(idx) && (entry >= 0 && entry < m_var_headers[idx].count);
}
