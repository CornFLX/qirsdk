#include "car.h"

int Car::id() const
{
    return m_id;
}

const QString &Car::name() const
{
    return m_name;
}

const QString &Car::className() const
{
    return m_class->name();
}

const QColor &Car::classColor() const
{
    return m_class->color();
}

const CarClass *Car::carClass() const
{
    return m_class;
}

Car::Car(int id, QString name, CarClass *car_class)
    : Multiton(id), m_id(id), m_name(name), m_class(car_class)
{
}

