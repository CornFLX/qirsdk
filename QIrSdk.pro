#-------------------------------------------------
#
# Project created by QtCreator 2016-10-11T16:00:09
#
#-------------------------------------------------

TARGET = QIrSdk
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    abstractirsdk.cpp \
    diskirsdk.cpp \
    licence.cpp \
    driver.cpp \
    car.cpp \
    entry.cpp \
    singleentry.cpp \
    teamentry.cpp \
    session.cpp \
    standing.cpp \
    carclass.cpp

HEADERS += \
    abstractirsdk.h \
    diskirsdk.h \
    licence.h \
    driver.h \
    multiton.h \
    car.h \
    entry.h \
    singleentry.h \
    teamentry.h \
    session.h \
    standing.h \
    carclass.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32 {
    SOURCES += liveirsdk.cpp
    HEADERS += liveirsdk.h
}
