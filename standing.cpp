#include "standing.h"

const Standing::TEntryPositionMap Standing::m_empty_entries = Standing::TEntryPositionMap();

Standing::Standing(const Session &session)
    : m_session(session)
{
}

const Standing::TEntryPositionMap &Standing::entries() const
{
    return m_entries;
}

const Standing::TEntryPositionMap &Standing::entries(const CarClass *car_class) const
{
    QHash<const CarClass*, TEntryPositionMap>::const_iterator it = m_class_entries.find(car_class);
    if (it == m_class_entries.cend()) {
        return m_empty_entries;
    }

    return it.value();
}

const Entry *Standing::entryAtPosition(int position) const
{
    return m_entries.key(position);
}

const Entry *Standing::entryAtPosition(int position, const CarClass *car_class) const
{
    return m_class_entries[car_class].key(position);
}

int Standing::positionOf(Entry *entry) const
{
    return m_entries[entry];
}

int Standing::classPositionOf(Entry *entry) const
{
    return m_class_entries[entry->carClass()][entry];
}

void Standing::addEntry(const Entry *entry)
{
    m_entries[entry] = 0;
    m_class_entries[entry->carClass()][entry] = 0;
}
