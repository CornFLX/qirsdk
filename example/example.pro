QT += core
QT -= gui

CONFIG += c++11

TARGET = example
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

unix|win32: LIBS += -L$$PWD/../ -lQIrSdk

INCLUDEPATH += $$PWD/../
DEPENDPATH += $$PWD/../

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../QIrSdk.lib
else:unix|win32-g++: PRE_TARGETDEPS += $$PWD/../libQIrSdk.a
