#include <QCoreApplication>

#include "diskirsdk.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    DiskIrSdk sdk;

    return a.exec();
}
