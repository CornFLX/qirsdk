#ifndef STANDING_H
#define STANDING_H

#include <QHash>
#include <QMap>
#include "carclass.h"
#include "entry.h"
#include "session.h"

class Standing
{
public:
    typedef QMap<const Entry*, int> TEntryPositionMap;

    Standing(const Session& session);

    Standing(const Standing&) = delete;
    Standing operator=(const Standing&) = delete;

    const TEntryPositionMap& entries() const;
    const TEntryPositionMap& entries(const CarClass* car_class) const;
    const Entry* entryAtPosition(int position) const;
    const Entry* entryAtPosition(int position, const CarClass* car_class) const;
    int positionOf(Entry *entry) const;
    int classPositionOf(Entry *entry) const;

    void addEntry(const Entry* entry);

private:
    const Session& m_session;
    TEntryPositionMap m_entries;
    QHash<const CarClass*, TEntryPositionMap> m_class_entries;

    static const TEntryPositionMap m_empty_entries;
};

#endif // STANDING_H
