#ifndef ABSTRACTIRSDK_H
#define ABSTRACTIRSDK_H

#include <QObject>
#include <QList>

class AbstractIrSdk : public QObject
{
    Q_OBJECT

protected:
    enum StatusField { CONNECTED = 1 };

    enum VarType {
        // 1 byte
        CHAR = 0,
        BOOL,

        // 4 bytes
        INT,
        BITFIELD,
        FLOAT,

        // 8 bytes
        DOUBLE,

        // index, don't use
        ETCOUNT
    };

    enum YamlState { SPACE, KEY, KEYSEP, VALUE, NEWLINE };

    static const int MAX_STRING = 32;
    static const int MAX_DESC   = 64;
    static const int MAX_BUFS   = 4;

    static const int MAX_SESSION_STR_VALUE = 64;

public:
    struct VarHeader {
        int type;   // VarType
        int offset; // Offset from start of buffer row
        int count;  // Number of entrys (array)

        int pad[1]; // (16 byte align)

        char name[MAX_STRING];
        char desc[MAX_DESC];
        char unit[MAX_STRING]; // Something like "kg/m^2"

        void clear() {
            type = 0;
            offset = 0;
            count = 0;
            memset(name, 0, sizeof(name));
            memset(desc, 0, sizeof(desc));
            memset(unit, 0, sizeof(unit));
        }
    };

    typedef QList<const VarHeader*> VarList;

    static const int WAIT_DATA_TIMEOUT = 1000/60;
    static const int TIMEOUT = 30;

    virtual bool isConnected() const = 0;
    bool started() const;
    virtual bool waitForData(int timeout = WAIT_DATA_TIMEOUT) = 0;

    virtual int getVarIdx(const char* name) = 0;

    // What is the base type of the data
    virtual VarType getVarType(int idx) = 0;
    VarType getVarType(const char* name);

    // How many elements in array, or 1 if not an array
    virtual int getVarCount(int idx) = 0;
    int getVarCount(const char* name);

    // idx is the variable index, entry is the array offset, or 0 if not an array element
    // will convert data to requested type
    virtual bool getVarBool(int idx, int entry = 0) = 0;
    bool getVarBool(const char* name, int entry = 0);

    virtual int getVarInt(int idx, int entry = 0) = 0;
    int getVarInt(const char* name, int entry = 0);

    virtual float getVarFloat(int idx, int entry = 0) = 0;
    float getVarFloat(const char* name, int entry = 0);

    virtual double getVarDouble(int idx, int entry = 0) = 0;
    double getVarDouble(const char* name, int entry = 0);

    // 1 success, 0 failure, -n minimum buffer size
    int getSessionStrVal(const char* path, char* val, int val_len) const;
    QString getSessionStrVal(const QString& path) const;
    virtual const char* getSessionInfo() const = 0;

    QVariant getVarVariant(int idx, int entry = 0);

    const char *getHeaderType(int idx) const;
    const char* getHeaderDesc(int idx) const;
    VarList getVarList() const;

public slots:
    void start(int timeout = WAIT_DATA_TIMEOUT);
    void stop();

protected:
    static const int   UNLIMITED_LAPS;
    static const float UNLIMITED_TIME;

    struct VarBuf {
        int tick_count; // Used to detect changes in data
        int buf_offset; // Offset from header
        int pad[2];     // (16 byte align)
    };

    struct Header {
        int ver;       // Api version 1 for now
        int status;    // Bitfield using StatusField
        int tick_rate; // Tick per seconds (60 or 360 etc)

        // session information, updated periodicaly
        int session_info_update; // Incremented when session info change
        int session_info_len;    // Length in bytes of session info string
        int session_info_offset; // Session info, encoded in YAML format

        // State data, output at tick_rate
        int num_vars;          // Length of array pointed to by var_header_offset
        int var_header_offset; // Offset to var_header[num_vars] array, describes the variables received in var_buf

        int num_buf;                    // IRSDK_MAX_BUFS (3 for now)
        int buf_len;                    // Length in bytes for one line
        int pad[2];                     // (16 byte align)
        VarBuf var_buf[MAX_BUFS]; // Buffers of data being written to
    };

    explicit AbstractIrSdk(QObject *parent);
    virtual ~AbstractIrSdk();

    virtual const VarHeader* getVarHeaderEntry(int index) const = 0;

    // Super simple YAML parser
    bool parseYaml(const char* data, const char* path, const char** val, int* len) const;

    Header* m_header;
    bool m_stop;
    bool m_started;

    char* m_strbuf;

signals:
    void dataReady();
};

#endif // ABSTRACTIRSDK_H
