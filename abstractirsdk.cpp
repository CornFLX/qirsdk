#include "abstractirsdk.h"

#include <QDebug>

const int   AbstractIrSdk::UNLIMITED_LAPS = 32767;
const float AbstractIrSdk::UNLIMITED_TIME = 604800.0f;

bool AbstractIrSdk::started() const
{
    return m_started;
}

AbstractIrSdk::VarType AbstractIrSdk::getVarType(const char *name)
{
    return getVarType(getVarIdx(name));
}

int AbstractIrSdk::getVarCount(const char *name)
{
    return getVarCount(getVarIdx(name));
}

bool AbstractIrSdk::getVarBool(const char *name, int entry)
{
    return getVarBool(getVarIdx(name), entry);
}

int AbstractIrSdk::getVarInt(const char *name, int entry)
{
    return getVarInt(getVarIdx(name), entry);
}

float AbstractIrSdk::getVarFloat(const char *name, int entry)
{
    return getVarFloat(getVarIdx(name), entry);
}

double AbstractIrSdk::getVarDouble(const char *name, int entry)
{
    return getVarDouble(getVarIdx(name), entry);
}

int AbstractIrSdk::getSessionStrVal(const char *path, char *val, int val_len) const
{
    if (isConnected() && path && val && val_len > 0) {
        const char* tval = nullptr;
        int tval_len = 0;
        if (parseYaml(getSessionInfo(), path, &tval, &tval_len)) {
            // Don't overflow out buffer
            int len = tval_len;
            if (len > val_len) len = val_len;

            // Copy what we can, even if buffer too small
            memcpy(val, tval, len);
            val[len] = '\0'; // original string has no null termination...

            // if buffer war big enough, return success
            if (val_len >= tval_len) return 1;
            else return -tval_len; // return size of buffer needed
        }
    }
    return 0;
}

QString AbstractIrSdk::getSessionStrVal(const QString &path) const
{
    QByteArray ba = path.toLocal8Bit();

    getSessionStrVal(ba.constData(), m_strbuf, MAX_SESSION_STR_VALUE);
    QString ret(m_strbuf);

    return ret;
}

QVariant AbstractIrSdk::getVarVariant(int idx, int entry)
{
    if (!isConnected()) return "";

    const VarHeader* h = getVarHeaderEntry(idx);
    switch (h->type) {
        case AbstractIrSdk::CHAR:
        return QVariant('?');

    case AbstractIrSdk::BOOL:
        return QVariant(getVarBool(idx, entry));

    case AbstractIrSdk::INT:
        return QVariant(getVarInt(idx, entry));

    case AbstractIrSdk::BITFIELD:
        return QVariant('?');

    case AbstractIrSdk::FLOAT:
        return QVariant(getVarFloat(idx, entry));

    case AbstractIrSdk::DOUBLE:
        return QVariant(getVarDouble(idx, entry));

    default:
        return QVariant('?');
    }
}

const char *AbstractIrSdk::getHeaderType(int idx) const
{
    if (!isConnected()) return "";

    const VarHeader* h = getVarHeaderEntry(idx);
    switch (h->type) {
        case AbstractIrSdk::CHAR:
        return "char";

    case AbstractIrSdk::BOOL:
        return "bool";

    case AbstractIrSdk::INT:
        return "int";

    case AbstractIrSdk::BITFIELD:
        return "bitfield";

    case AbstractIrSdk::FLOAT:
        return "float";

    case AbstractIrSdk::DOUBLE:
        return "double";

    default:
        return "unknown";
    }
}

const char *AbstractIrSdk::getHeaderDesc(int idx) const
{
    if (!isConnected()) return "";
    return getVarHeaderEntry(idx)->desc;
}

AbstractIrSdk::VarList AbstractIrSdk::getVarList() const
{
    VarList varlist;
    if (!isConnected()) return varlist;

    varlist.reserve(m_header->num_vars);
    for (int idx = 0; idx < m_header->num_vars; ++idx) {
        varlist.push_back(getVarHeaderEntry(idx));
    }

    return varlist;
}


void AbstractIrSdk::start(int timeout)
{
    Q_ASSERT(!m_started);
    m_stop = false;
    while (!m_stop) {
        m_started = true;
        if (waitForData(timeout))
            emit dataReady();
    }
    m_started = false;
}

void AbstractIrSdk::stop()
{
    m_stop = true;
}

AbstractIrSdk::AbstractIrSdk(QObject *parent) : QObject(parent), m_header(0), m_stop(false), m_started(false), m_strbuf(nullptr)
{
    m_strbuf = new char[MAX_SESSION_STR_VALUE];
}

AbstractIrSdk::~AbstractIrSdk()
{
    if (m_header) {
        delete m_header;
    }
    delete[] m_strbuf;
}

bool AbstractIrSdk::parseYaml(const char *data, const char *path, const char **val, int *len) const
{
    if (!data || !path || !val || !len) return false;

    // Make sure we set this to something
    *val = 0;
    *len = 0;

    int depth = 0;
    YamlState state = SPACE;

    const char* keystr = 0;
    int keylen = 0;

    const char* valuestr = 0;
    int valuelen = 0;

    const char* pathptr = path;
    int pathdepth = 0;

    while (*data) {
        switch (*data) {
        case ' ':
            if (state == NEWLINE) state = SPACE;
            if (state == SPACE) ++depth;
            else if (state == KEY) ++keylen;
            else if (state == VALUE) ++valuelen;
            break;

        case '-':
            if (state == NEWLINE) state = SPACE;
            if (state == SPACE) ++depth;
            else if (state == KEY) ++keylen;
            else if (state == VALUE) ++valuelen;
            else if (state == KEYSEP) {
                state = VALUE;
                valuestr = data;
                valuelen = 1;
            }
            break;

        case ':':
            if (state == KEY) {
                state = KEYSEP;
                ++keylen;
            } else if (state == KEYSEP) {
                state = VALUE;
                valuestr = data;
            } else if (state == VALUE) ++valuelen;
            break;

        case '\n':
        case '\r':
            if (state != NEWLINE) {
                if (depth < pathdepth) return false;
                else if (keylen && 0 == strncmp(keystr, pathptr, keylen)) {
                    bool found = true;

                    // Do we need to test the value ?
                    if (*(pathptr+keylen) == '{') {
                        // Search for closing brace
                        int pathvaluelen = keylen + 1;
                        while (*(pathptr+pathvaluelen) && *(pathptr+pathvaluelen) != '}') ++pathvaluelen;

                        if (valuelen == pathvaluelen - (keylen+1) && 0 == strncmp(valuestr, (pathptr+keylen+1), valuelen)) pathptr += (valuelen + 2);
                        else found = false;
                    }

                    if (found) {
                        pathptr += keylen;
                        pathdepth = depth;

                        if (*pathptr == '\0') {
                            *val = valuestr;
                            *len = valuelen;
                            return true;
                        }
                    }
                }

                depth = 0;
                keylen = 0;
                valuelen = 0;
            }
            state = NEWLINE;
            break;

        default:
            if (state == SPACE || state == NEWLINE) {
                state = KEY;
                keystr = data;
                keylen = 0; // redundant ?
            } else if (state == KEYSEP) {
                state = VALUE;
                valuestr = data;
                valuelen = 0; // redundant ?
            }
            if (state == KEY) ++keylen;
            if (state == VALUE) ++valuelen;
            break;
        }
        ++data;
    }

    return false;
}
