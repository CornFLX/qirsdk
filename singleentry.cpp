#include "singleentry.h"


SingleEntry::SingleEntry(Driver *drv, Car *car)
    : Entry(car), m_driver(drv)
{
}

Driver *SingleEntry::currentDriver() const
{
    return m_driver;
}

Entry::DriverList SingleEntry::drivers() const
{
    return DriverList({m_driver});
}

int SingleEntry::irating() const
{
    return m_driver->irating();
}

const QString &SingleEntry::name() const
{
    return m_driver->name();
}
