#ifndef ENTRY_H
#define ENTRY_H

#include <QList>
#include <QString>

#include "car.h"
#include "driver.h"

class Entry
{
public:
    typedef QList<Driver*> DriverList;

    virtual Driver* currentDriver() const = 0;
    virtual DriverList drivers() const = 0;
    virtual int irating() const = 0;
    virtual const QString& name() const = 0;

    const CarClass* carClass() const;

    virtual ~Entry() {}
protected:
    Entry(Car* car);

    Car* m_car;
};

#endif // ENTRY_H
