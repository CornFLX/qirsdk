#include "driver.h"

Driver::Driver(QString name, int irating)
    : m_name(name), m_irating(irating)
{

}

const QString &Driver::name() const
{
    return m_name;
}

int Driver::irating() const
{
    return m_irating;
}

QString Driver::licence() const
{
    return m_licence;
}

double Driver::safetyRating() const
{
    return m_licence.safetyRating();
}

QString Driver::licenceType() const
{
    return m_licence.type();
}
