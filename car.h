#ifndef CAR_H
#define CAR_H

#include "multiton.h"

#include <QString>
#include "carclass.h"

class Car : public Multiton<int, Car>
{
public:
    int id() const;
    const QString& name() const;
    const QString& className() const;
    const QColor& classColor() const;
    const CarClass* carClass() const;

    friend class Multiton<int, Car>;

protected:
    Car(int id, QString name, CarClass* car_class);

private:
    int m_id;
    QString m_name;
    CarClass* m_class;
};

#endif // CAR_H
