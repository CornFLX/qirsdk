#ifndef DISKIRSDK_H
#define DISKIRSDK_H

#include "abstractirsdk.h"

class DiskIrSdk : public AbstractIrSdk
{
    Q_OBJECT
public:
    DiskIrSdk(QObject* parent = 0);
    DiskIrSdk(const char* ibt_path, QObject* parent = 0);

    ~DiskIrSdk();

    bool isFileOpen() const;
    bool openFile(const char* path);
    void closeFile();

    bool isConnected() const;
    bool waitForData(int timeout = WAIT_DATA_TIMEOUT);
    int getVarIdx(const char* namen);
    VarType getVarType(int idx);

    int getVarCount(int idx);
    using AbstractIrSdk::getVarCount;

    bool getVarBool(int idx, int entry = 0);
    using AbstractIrSdk::getVarBool;

    int getVarInt(int idx, int entry = 0);
    using AbstractIrSdk::getVarInt;

    float getVarFloat(int idx, int entry = 0);
    using AbstractIrSdk::getVarFloat;

    double getVarDouble(int idx, int entry = 0);
    using AbstractIrSdk::getVarDouble;

    const char* getSessionInfo() const;

protected:
    struct SubHeader {
        time_t session_start_date;
        double session_start_time;
        double session_end_time;
        int session_lap_count;
        int session_record_count;
    };

    FILE* m_ibt_file;

    SubHeader* m_subheader;

    VarHeader* m_var_headers;
    char* m_var_buf;
    char* m_session_info;

    const VarHeader* getVarHeaderEntry(int index) const;

    bool isValidIdx(int idx) const;
    bool isValidEntry(int idx, int entry) const;
};

#endif // DISKIRSDK_H
