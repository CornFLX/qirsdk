#include "liveirsdk.h"

#include <windows.h>
#include <QThread>

LiveIrSdk::LiveIrSdk(QObject *parent)
    : AbstractIrSdk(parent), m_data_valid_event(0), m_memmap(0), m_shared_mem(0), m_last_tick_count(INT_MAX), m_initialized(false), m_last_valid_time(0), m_data(0), m_data_len(0), m_status_id(0)
{
    qDebug() << "new LiveIrSdk";
}

LiveIrSdk::~LiveIrSdk()
{
    shutdown();
}

bool LiveIrSdk::isConnected() const
{
    if (!m_data || !m_initialized) return false;

    int elapsed = (int)difftime(time(0), m_last_valid_time);

    return (m_header->status & CONNECTED) > 0 && elapsed < TIMEOUT;
}

bool LiveIrSdk::waitForData(int timeout)
{
    // Wait for start of session or new data
    if (waitForDataReady(timeout)) {
        Q_ASSERT(m_header);

        // If new connection, or data changed length then init
        if (!m_data || m_data_len != m_header->buf_len) {
            // Allocate memory to hold incoming data from sim
            if (m_data) delete []m_data;
            m_data_len = m_header->buf_len;
            m_data = new char[m_data_len];

            // Indicate a new connection
            ++m_status_id;

            // And try to fill in the data
            if (getNewData()) return true;
        } else if (m_data) {
            // Else we are already initialized, and data is ready for processing
            return true;
        }
    } else if (!isConnected()) {
        // Else session ended
        if (m_data) delete []m_data;
        m_data = 0;
    }
    return false;
}

int LiveIrSdk::getVarIdx(const char *name)
{
    if (!isConnected()) return 0;

    if (!m_varname_idx.contains(name)) {
        const VarHeader* var;
        int index = -1;
        for (int i = 0; i < m_header->num_vars; ++i) {
            var = getVarHeaderEntry(i);
            if (var && 0 == strncmp(name, var->name, MAX_STRING)) {
                index = i;
                break;
            }
        }
        m_varname_idx[name] = index;
        return index;
    }

    return m_varname_idx[name];
}

AbstractIrSdk::VarType LiveIrSdk::getVarType(int idx)
{
    if (!isConnected()) return CHAR;

    const VarHeader* vh = getVarHeaderEntry(idx);
    Q_ASSERT(vh);

    return (AbstractIrSdk::VarType)vh->type;
}

int LiveIrSdk::getVarCount(int idx)
{
    if (!isConnected()) return 0;

    const VarHeader* vh = getVarHeaderEntry(idx);
    Q_ASSERT(vh);

    return vh->count;
}

bool LiveIrSdk::getVarBool(int idx, int entry)
{
    if (isConnected()) {
        const VarHeader* vh = getVarHeaderEntry(idx);
        Q_ASSERT(isValidOffset(vh, entry));

        const char* data = m_data + vh->offset;
        switch (vh->type) {
        case CHAR:
        case BOOL:
            return (((const char*)data)[entry]) != 0;

        case INT:
        case BITFIELD:
            return (((const int*)data)[entry]) != 0;

        case FLOAT:
            return (((const float*)data)[entry]) >= 1.0f;

        case DOUBLE:
            return (((const double*)data)[entry]) > 1.0;
        }
    }

    return false;
}

int LiveIrSdk::getVarInt(int idx, int entry)
{
    if (isConnected()) {
        const VarHeader* vh = getVarHeaderEntry(idx);
        Q_ASSERT(isValidOffset(vh, entry));

        const char* data = m_data + vh->offset;

        switch (vh->type) {
        case CHAR:
        case BOOL:
            return (int)(((const char*)data)[entry]);

        case INT:
        case BITFIELD:
            return (int)(((const int*)data)[entry]);

        case FLOAT:
            return (int)(((const float*)data)[entry]);

        case DOUBLE:
            return (int)(((const double*)data)[entry]);
        }
    }

    return 0;
}

float LiveIrSdk::getVarFloat(int idx, int entry)
{
    if (isConnected()) {
        const VarHeader* vh = getVarHeaderEntry(idx);
        Q_ASSERT(isValidOffset(vh, entry));

        const char* data = m_data + vh->offset;
        switch (vh->type) {
        case CHAR:
        case BOOL:
            return (float)(((const char*)data)[entry]);

        case INT:
        case BITFIELD:
            return (float)(((const int*)data)[entry]);

        case FLOAT:
            return (float)(((const float*)data)[entry]);

        case DOUBLE:
            return (float)(((const double*)data)[entry]);
        }
    }

    return 0.0f;
}

double LiveIrSdk::getVarDouble(int idx, int entry)
{
    if (isConnected()) {
        const VarHeader* vh = getVarHeaderEntry(idx);
        Q_ASSERT(isValidOffset(vh, entry));

        const char* data = m_data + vh->offset;
        switch (vh->type) {
        case CHAR:
        case BOOL:
            return (double)(((const char*)data)[entry]);

        case INT:
        case BITFIELD:
            return (double)(((const int*)data)[entry]);

        case FLOAT:
            return (double)(((const float*)data)[entry]);

        case DOUBLE:
            return (double)(((const double*)data)[entry]);
        }
    }

    return 0.0;
}

bool LiveIrSdk::initialize()
{
    if (!m_memmap) {
        m_memmap = OpenFileMapping(FILE_MAP_READ, FALSE, L"Local\\IRSDKMemMapFileName");
        m_last_tick_count = INT_MAX;
    }
    if (!m_memmap) return false;

    if (!m_shared_mem) {
        m_shared_mem = (const char*)MapViewOfFile(m_memmap, FILE_MAP_READ, 0, 0, 0);
        m_header = (Header*)m_shared_mem;
        m_last_tick_count = INT_MAX;
    }
    Q_ASSERT(m_shared_mem);

    if (!m_data_valid_event) {
        m_data_valid_event = OpenEvent(SYNCHRONIZE, false, L"Local\\IRSDKDataValidEvent");
        m_last_tick_count = INT_MAX;
    }
    Q_ASSERT(m_data_valid_event);

    m_initialized = true;
    return true;
}

bool LiveIrSdk::waitForDataReady(int timeout)
{
    if (m_initialized || initialize()) {
        // Just to be sure, check before we sleep
        if (getNewData()) return true;

        // Sleep till signaled
        WaitForSingleObject(m_data_valid_event, timeout);

        // We woke up, so check for data
        return getNewData();
    } 
    
    // Sleep if error
    if (timeout) QThread::msleep(timeout);
    return false;
}

bool LiveIrSdk::getNewData()
{
    if (!m_initialized && !initialize()) return false;
    
    Q_ASSERT(m_header != 0);
    
    // If sim is not active, the no new data
    if (!(m_header->status & CONNECTED)) {
        m_last_tick_count = INT_MAX;
        return false;
    }
    
    int latest = 0;
    for (int i = 1; i < m_header->num_buf; ++i) {
        if (m_header->var_buf[latest].tick_count < m_header->var_buf[i].tick_count) 
            latest = i;
    }
    
    // If newer than last received, then report new data
    if (m_last_tick_count < m_header->var_buf[latest].tick_count) {
        // If needed to retrieve data
        if (m_data) {
            // Try twice to get the data out
            for (int count = 0; count < 2; ++count) {
                int cur_tick_count = m_header->var_buf[latest].tick_count;
                memcpy(m_data, m_shared_mem + m_header->var_buf[latest].buf_offset, m_header->buf_len);

                if (cur_tick_count == m_header->var_buf[latest].tick_count) {
                    m_last_tick_count = cur_tick_count;
                    m_last_valid_time = time(0);
                    return true;
                }
            }

            // If here, the data changed out from under us
            return false;

        } else {
            m_last_tick_count = m_header->var_buf[latest].tick_count;
            m_last_valid_time = time(0);
            return true;
        }
    // If older than last received, than reset, we probably disconnected
    } else if (m_last_tick_count > m_header->var_buf[latest].tick_count) {
        m_last_tick_count = m_header->var_buf[latest].tick_count;

        return false;
    }
    // Else the same, and nothing changed this tick

    return false;
}

const char *LiveIrSdk::getSessionInfo() const
{
    if (m_initialized) return m_shared_mem + m_header->session_info_offset;
    return 0;
}

void LiveIrSdk::shutdown()
{
    if (m_data_valid_event) CloseHandle(m_data_valid_event);
    if (m_shared_mem) UnmapViewOfFile(m_shared_mem);
    if (m_memmap) CloseHandle(m_memmap);

    m_data_valid_event = 0;
    m_shared_mem = 0;
    m_header = 0;
    m_memmap = 0;

    m_initialized = false;
    m_last_tick_count = INT_MAX;

    if (m_data) delete[] m_data;
    m_data = 0;
}

const AbstractIrSdk::VarHeader *LiveIrSdk::getVarHeaderEntry(int index) const
{
    if (m_initialized && isValidIdx(index)) {
        return &((VarHeader*)(m_shared_mem + m_header->var_header_offset))[index];
    }

    return 0;
}

bool LiveIrSdk::isValidIdx(int idx) const
{
    return (idx >= 0 && idx < m_header->num_vars);
}

bool LiveIrSdk::isValidOffset(const AbstractIrSdk::VarHeader *vh, int entry) const
{
    return vh && entry >= 0 && entry < vh->count;
}
