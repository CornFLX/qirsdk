#ifndef SESSION_H
#define SESSION_H

#include <QString>

class Session
{
public:
    static const int UNLIMITED_LAPS = 32767;

    Session(const QString& type, int time_elapsed=0, int laps_elapsed=0, int time_remain=0, int laps_remain=0);

    Session(const Session&) = delete;
    Session operator=(const Session&) = delete;

    const QString& type() const;
    int timeRemain() const;
    int lapsRemain() const;
    int timeElapsed() const;
    int lapsElapsed() const;

private:
    QString m_type;

    int m_time_remain;
    int m_time_elapsed;
    int m_laps_remain;
    int m_laps_elapsed;
};

#endif // SESSION_H
