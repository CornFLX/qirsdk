#include "licence.h"

Licence::Licence()
    : m_type("R"), m_safety_rating(0.0)
{

}

const QString& Licence::type() const
{
    return m_type;
}

double Licence::safetyRating() const
{
    return m_safety_rating;
}

Licence::operator QString() const
{
    return m_type + QString::number(m_safety_rating, 'f', 2);
}
